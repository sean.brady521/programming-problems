function _isBetterPalindrome (strA: string, strB: string) {
  if (strA.length > strB.length) return true
  else if (strA.length === strB.length && strA < strB) return true
  else return false
}

function _reverseString(str: string) {
    return str.split("").reverse().join("");
}

function _getMaxPalindromeFromPos (strA: string, strB: string, posA: number, posB: number): string | null {
  //console.log(`      checking position A:${posA} | B:${posB}}`)
  let palindromePart = ''
  let indexOffset = 0
  while(true) {
    if (posA - indexOffset < 0 || posB + indexOffset >= strB.length) break
    if (strA[posA - indexOffset] !== strB[posB + indexOffset]) break

    palindromePart += strA[posA - indexOffset]
    //console.log(`        matched on ${strA[posA]} at A:${posA - indexOffset} | B:${posB + indexOffset}, palindrome part is now ${palindromePart}`)
    indexOffset++
  }

  const innerStr = `${strA}${strB}`.slice(posA + 1, posB + strA.length)
  let bestInnerStr = innerStr[0]
  let consecCounter = 1

  for (let i = 1; i <= innerStr.length; i++) {
    if (innerStr[i] === innerStr[i - 1]) consecCounter++
    else {
      let str = ''
      for (let iL = 0; iL < consecCounter; iL++) {
        str+=innerStr[i - 1]
      }
      //console.log(`checking inner ${str}`)
      if (_isBetterPalindrome(str, bestInnerStr)) bestInnerStr = str
      consecCounter = 1
    }
  }

  //console.log(`      inner string is ${innerStr} and the best combo is ${bestInnerStr}`)

  // let maxInnerLetter = ''
  // const aInner = strA[posA + 1]
  // const bInner = strB[posB - 1]
  // if (aInner && aInner < bInner) maxInnerLetter = aInner
  // if (aInner && !bInner) maxInnerLetter = aInner
  // else if (bInner && bInner < aInner) maxInnerLetter = bInner
  // else if (bInner && !aInner) maxInnerLetter = bInner
  // else if (aInner && bInner && aInner === bInner) maxInnerLetter = aInner

  // let maxInnerLetter = ''
  // // //console.log({combined: `${strA}${strB}`, from: posA + 1, posB})
  // const innerStr = `${strA}${strB}`.slice(posA + 1, posB + strA.length)
  // //console.log(`      inner string is ${innerStr}`)
  // for (const letter of innerStr) {
  //   if (letter < maxInnerLetter || !maxInnerLetter) maxInnerLetter = letter
  // }

  const palindrome = `${_reverseString(palindromePart)}${bestInnerStr}${palindromePart}`
  //console.log(`      best is ${palindrome}`)
  return palindrome
}

function _findPalindromeWithStartLetter(strA: string, strB: string, targetLetter: string): string | null {
  const letterPositionsA = new Set<number>()
  const letterPositionsB = new Set<number>()

  for (let i = 0; i < strA.length; i++) {
    if (strA[i] === targetLetter) letterPositionsA.add(i)
  }

  for (let i = 0; i < strB.length; i++) {
    if (strB[i] === targetLetter) letterPositionsB.add(i)
  }

  let bestPalindrome = ''
  for (const posA of letterPositionsA) {
    for (const posB of letterPositionsB) {
      const palindrome = _getMaxPalindromeFromPos(strA, strB, posA, posB)
      // //console.log(`      starting at A:${posA} and B:${posB} we get: ${palindrome}`)
      if (palindrome !== null && _isBetterPalindrome(palindrome, bestPalindrome)) {
        bestPalindrome = palindrome
      }
    }
  }

  return bestPalindrome || null
}

function buildPalindrome(a: string, b: string): string {
  // Find common letters
  const letterOccurencesA: Record<string, number> = {}
  const letterOccurencesB: Record<string, number> = {}
  for (const letter of a) {
    if (!letterOccurencesA[letter]) letterOccurencesA[letter] = 0
    letterOccurencesA[letter]++
  }

  for (const letter of b) {
    if (!letterOccurencesB[letter]) letterOccurencesB[letter] = 0
    letterOccurencesB[letter]++
  }

  const commonLetters = new Set<string>()
  for (const letter of Object.keys(letterOccurencesA)) {
    if (letterOccurencesA[letter] && letterOccurencesB[letter]) commonLetters.add(letter)
  }

  //console.log(`common letters are`)
  //console.log(commonLetters)

  // For each common letter identify longest reverse match
  let bestPalindrome = ''
  for (const letter of commonLetters) {
    //console.log(`Checking best palindrome starting at letter ${letter}`)
    const palindrome = _findPalindromeWithStartLetter(a, b, letter)
    //console.log(`Best palindrome for ${letter} is ${palindrome}`)
    if (palindrome !== null && _isBetterPalindrome(palindrome, bestPalindrome)) { 
      //console.log(`better than existing`)
      bestPalindrome = palindrome
    }
  }

  return bestPalindrome || '-1'

}

const sampleInput: [string, string][] = [
  ['bac', 'bac'],
  ['abc', 'def'],
  ['jdfh', 'fds'],
  ['ottloictodtdtloloollllyocidyiodttoacoctcdcidcdttyoiilocltacdlydaailaiylcttilld', 'jevgfsuujwrunvgvgwpfbknkruvwzgxxgksmexqvxbghfffseuugxkwexhzfbpu'],
  ['qquhuwqhdswxxrxuzzfhkplwunfagppcoildagktgdarveusjuqfistulgbglwmfgzrnyxryetwzhlnfewczmnoozlqatugmd', 'jwgzcfabbkoxyjxkatjmpprswkdkobdagwdwxsufeesrvncbszcepigpbzuzoootorzfskcwbqorvw'],
  ['dczatfarqdkelalxzxillkfdvpfpxabqlngdscrentzamztvvcvrtcm', 'bqlizijdwtuyfrxolsysxlfebpolcmqsppmrfkyunydtmwbexsngxhwvroandfqjamzkpttslildlrkjoyrpxugiceahgiakev'],
  ['kfnfolpcfblpncetyhtrwxkbosccskxbuvcrosavnpxzoeoyyghbbqkflslfkqbbhgyyjj', 'qrxpxnloeozxpnvasorcvubxksccsobkxwrthytecnplbfcplofx'],
  ['mlfcpidlqrvngnvttaifcbopnwezesomkxhaiafmvkbjaisyr', 'btultpnxbcrmornqumatserhieqggrivouwfnbnghdfall'],
  ['pb', 'kkb'],
  ['rfq', 'xzj'],
  ['zlc', 'zdw'],
  ['s', 'k']
]

const sampleOutput: string[] = [
  'aba',
  '-1',
  'dfhfd',
  '-1',
  'oozlzoo',
  'lxsysxl',
  'folpcfblpncetyhtrwxkbosccskxbuvcrosavnpxzoeoyyghbbqkflslfkqbbhgyyoeozxpnvasorcvubxksccsobkxwrthytecnplbfcplof',
  'rvngnvr',
  'bkkb',
  '-1',
  'zlz',
  '-1',
  '-1',
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const [a,b]: string[] = input
  const actual = buildPalindrome(a, b) 

  console.log({ expected, actual, pass: expected === actual })
}