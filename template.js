function template (myParam) {
  console.log(myParam)
}

const sampleInput = []

const sampleOutput = []

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const { myParam } = input
  const actual = template(myParam) 

  console.log({ expected, actual, pass: expected === actual })
}
