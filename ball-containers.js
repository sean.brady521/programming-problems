function organizingContainers (containers) {
  console.log('Organising')
  console.log(containers)
  // count how many in each container
  const ballCount = {}
  for (const container of containers) {
    const totalBalls = container.reduce((a, b) => a + b, 0)

    if (!ballCount[totalBalls]) ballCount[totalBalls] = 0
    ballCount[totalBalls]++
  }

  // Count how many we have of each type
  const typeCount = {}
  for (const container of containers) {
    for (let typeNo = 0; typeNo < container.length; typeNo++) {
      if (!typeCount[typeNo]) typeCount[typeNo] = 0
      typeCount[typeNo] += container[typeNo]
    }
  }

  console.log({ ballCount, typeCount })

  // Check that we can fit each type into a container
  for (const [type, count] of Object.entries(typeCount)) {
    console.log(`allocating ${type} to a container of size ${count}`)
    if (ballCount[count]) { // Found a container for it
      ballCount[count]--
      console.log(`Success... ${ballCount[count]} of size ${count} remaining`)
    } else {
      console.log('Failed, none left')
      return 'Impossible'
    }
  }

  return 'Possible'
}

const sampleInput = [
  [[1, 1], [1, 1]],
  [[0, 2], [1, 1]],
  [[1, 3, 1], [2, 1, 2], [3, 3, 3]],
  [[0, 2, 1], [1, 1, 1], [2, 0, 0]]
]

for (const input of sampleInput) {
  console.log(organizingContainers(input))
}
