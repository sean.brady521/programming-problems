function _createRankArray (ranked) {
  if (!ranked.length) return []

  let currRank = 1
  let currScore = ranked[0]

  const rankArray = []
  for (const score of ranked) {
    if (score !== currScore) {
      currRank++
      currScore = score
    }
    rankArray.push(currRank)
  }

  return rankArray
}

// Designed for DESCENDING lists
function _binaryFindInsertIdx (arr, value, _maxIdx) {
  const debug = value === 29
  let maxIdx = _maxIdx || (arr.length - 1)
  let minIdx = 0
  while (minIdx <= maxIdx) {
    const middleIdx = Math.floor((maxIdx + minIdx) / 2)
    const middleVal = arr[middleIdx]
    if (debug) console.log({ middleIdx, middleVal, maxIdx, minIdx })
    if (maxIdx === minIdx) {
      let result
      if (middleVal === value) {
        result = middleIdx
      } else if (middleVal > value) {
        result = middleIdx + 1
      } else {
        result = middleIdx
      }
      return result
    } else if (middleVal > value) {
      minIdx = middleIdx + 1
    } else if (middleVal < value) {
      maxIdx = middleIdx - 1
    } else {
      return middleIdx
    }
  }
  return minIdx
}

function climbingLeaderboard (scores, player) {
  // Start by creating another array, a ranking array, where index of score = rank in this array
  // Given the new score, perform a binary search to find the new position
  // Binary search insertion where the new index + 1 is your new rank
  // Given that the player scores are in ascending order, we can disregard a portion of the array each time
  const rankArray = _createRankArray(scores)
  // console.log({ rankArray, scores, player })
  const results = []
  let maxIdx = scores.length - 1
  for (const playerScore of player) {
    const destIdx = _binaryFindInsertIdx(scores, playerScore, maxIdx)
    let result
    if (destIdx === -1) {
      result = 1
    } else if (destIdx === scores.length) {
      result = rankArray[rankArray.length - 1] + 1
    } else {
      result = rankArray[destIdx]
    }
    maxIdx = destIdx
    results.push(result)
    console.log({ playerScore, destIdx, maxIdx })
  }
  return results
}

const sampleInput = [
  // { ranked: [100, 90, 90, 80], player: [70, 80, 105] },
  // { ranked: [100, 100, 50, 40, 40, 20, 10], player: [5, 25, 50, 120] },
  // { ranked: [100, 90, 90, 80, 75, 60], player: [50, 65, 77, 90, 102] },
  {
    ranked: [295, 294, 291, 287, 287, 285, 285, 284, 283, 279, 277, 274, 274, 271, 270, 268, 268, 268, 264, 260, 259, 258, 257, 255, 252, 250, 244, 241, 240, 237, 236, 236, 231, 227, 227, 227, 226, 225, 224, 223, 216, 212, 200, 197, 196, 194, 193, 189, 188, 187, 183, 182, 178, 177, 173, 171, 169, 165, 143, 140, 137, 135, 133, 130, 130, 130, 128, 127, 122, 120, 116, 114, 113, 109, 106, 103, 99, 92, 85, 81, 69, 68, 63, 63, 63, 61, 57, 51, 47, 46, 38, 30, 28, 25, 22, 15, 14, 12, 6, 4],
    player: [5, 5, 6, 14, 19, 20, 23, 25, 29, 29, 30, 30, 32, 37, 38, 38, 38, 41, 41, 44, 45, 45, 47, 59, 59, 62, 63, 65, 67, 69, 70, 72, 72, 76, 79, 82, 83, 90, 91, 92, 93, 98, 98, 100, 100, 102, 103, 105, 106, 107, 109, 112, 115, 118, 118, 121, 122, 122, 123, 125, 125, 125, 127, 128, 131, 131, 133, 134, 139, 140, 141, 143, 144, 144, 144, 144, 147, 150, 152, 155, 156, 160, 164, 164, 165, 165, 166, 168, 169, 170, 171, 172, 173, 174, 174, 180, 184, 187, 187, 188, 194, 197, 197, 197, 198, 201, 202, 202, 207, 208, 211, 212, 212, 214, 217, 219, 219, 220, 220, 223, 225, 227, 228, 229, 229, 233, 235, 235, 236, 242, 242, 245, 246, 252, 253, 253, 257, 257, 260, 261, 266, 266, 268, 269, 271, 271, 275, 276, 281, 282, 283, 284, 285, 287, 289, 289, 295, 296, 298, 300, 300, 301, 304, 306, 308, 309, 310, 316, 318, 318, 324, 326, 329, 329, 329, 330, 330, 332, 337, 337, 341, 341, 349, 351, 351, 354, 356, 357, 366, 369, 377, 379, 380, 382, 391, 391, 394, 396, 396, 400]

  }
]

for (const input of sampleInput) {
  const { ranked, player } = input
  console.log(climbingLeaderboard(ranked, player))
}
