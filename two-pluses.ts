interface Plus {
  origin: [number, number]
  offset: number
  area: number
}

interface Combo {
  l: number
  lVal: Plus 
  s: number
  sVal: Plus
  total: number
}

class comboIterator {
  private l: number = 0 // larger
  private s: number = 0 // smaller
  private arr: Plus[]
  private sMap: Record<number, true> = {}

  constructor (arr: Plus[]) {
    this.arr = arr
  }

  next () {
    const { l, s, arr, sMap } = this
    let shiftLResult: Combo
    if (l + 1 < s) {
      // If we're shifting large forward, let's try and shift small back if it doesn't bump into large
      shiftLResult = l + 1 < s - 1
        ? { l: l + 1, s: s - 1, lVal: arr[l + 1], sVal: arr[s - 1], total: arr[l + 1].area * arr[s - 1].area }
        : { l: l + 1, s: s, lVal: arr[l + 1], sVal: arr[s],  total: arr[l + 1].area * arr[s].area }
    }

    let shiftSResult: Combo
    
    if (s + 1 < arr.length) {
      shiftSResult = { 
        l,
        s: s + 1,
        lVal: arr[l],
        sVal: arr[s + 1],
        total: arr[l].area * arr[s + 1].area
      }
    }

    if (!shiftLResult && !shiftSResult) return null

    if (!shiftLResult || shiftSResult?.total > shiftLResult?.total)  {
      this.s = shiftSResult.s
      
      // If we reach a new s we want to reset l
      if (!sMap[shiftSResult.s]) {
        sMap[shiftSResult.s] = true
        this.l = 0
        shiftSResult.l = 0
        shiftSResult.lVal = arr[0]
      }

      return shiftSResult
    } else {
      this.s = shiftLResult.s
      this.l = shiftLResult.l
      return shiftLResult
    }

  }
}

function straightFloodFill(x: number, y: number, grid: string[]) {
  const origin: [number, number] = [x,y]
  const pluses: Plus[] = []
  let offset = 0 
  while (true) {
    const neighbours = [[x + offset, y], [x - offset, y], [x, y + offset], [x, y - offset]]
    // console.log({neighbours})
    const validPlus = neighbours.every(pos => grid[pos[1]]?.[pos[0]] === 'G')
    if (validPlus) {
      pluses.push({ origin, offset, area: offset * 4 + 1 })
      offset++
    } else {
      break
    }
  }
  return pluses
}

function twoPluses(grid: string[]): number {
  const allPluses: Plus[] = []
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[0].length; x++) {
      const pluses = straightFloodFill(x, y, grid)
      for (const plus of pluses) {
        allPluses.push(plus)
      }
    }
  }

  const sortedPluses = allPluses.sort((a: Plus, b: Plus) => b.area - a.area)
  // console.log(sortedPluses)
  const iterator = new comboIterator(sortedPluses)
  while(true) {
    const res = iterator.next()
    if (res === null) break

    console.log(res)

    const plusA = sortedPluses[res.l]
    const plusB = sortedPluses[res.s]

    const isValid = checkIsValidCombo(plusA, plusB)
    if (isValid) return res.total
  }
  // console.log('NUTHIN')
  return 0
}

function checkIsValidCombo (plusA: Plus, plusB: Plus): boolean {
  let debug = false
  // if (plusA.area + plusB.area === 22) debug = true

  const xRangeA = [plusA.origin[0] - plusA.offset, plusA.origin[0] + plusA.offset]
  const yRangeA = [plusA.origin[1] - plusA.offset, plusA.origin[1] + plusA.offset]

  const xRangeB = [plusB.origin[0] - plusB.offset, plusB.origin[0] + plusB.offset]
  const yRangeB = [plusB.origin[1] - plusB.offset, plusB.origin[1] + plusB.offset]

  const marked: Record<string, true> = {}

  // Fill marked
  for (let x = xRangeA[0]; x <= xRangeA[1]; x++) {
    const locKey = `${x}|${plusA.origin[1]}`
    marked[locKey] = true
  }

  for (let y = yRangeA[0]; y <= yRangeA[1]; y++) {
    const locKey = `${plusA.origin[0]}|${y}`
    marked[locKey] = true
  }

  // Check for conflicts
  for (let x = xRangeB[0]; x <= xRangeB[1]; x++) {
    const locKey = `${x}|${plusB.origin[1]}`
    if (marked[locKey]) { 
      if (debug) {
        console.log({
          conflict: {x, y: plusB.origin[1]},
          plusA,
          plusB
        })
      }
      return false
    }
  }

  for (let y = yRangeB[0]; y <= yRangeB[1]; y++) {
    const locKey = `${plusB.origin[0]}|${y}`
      if (debug) {
        console.log({
          conflict: {x: plusB.origin[0], y},
          plusA,
          plusB
        })
      }
    if (marked[locKey]) return false
  }

  return true
}

const sampleInput = [
  // [
  //   'GGGGGG',
  //   'GBBBGB',
  //   'GGGGGG',
  //   'GGBBGB',
  //   'GGGGGG'
  // ],
  // [
  //   'BGBBGB',
  //   'GGGGGG',
  //   'BGBBGB',
  //   'GGGGGG',
  //   'BGBBGB',
  //   'BGBBGB',
  // ],
  // [
  //   'GGGGGGGG',
  //   'GBGBGGBG',
  //   'GBGBGGBG',
  //   'GGGGGGGG',
  //   'GBGBGGBG',
  //   'GGGGGGGG',
  //   'GBGBGGBG',
  //   'GGGGGGGG',
  // ],
  // [
  //   'GBGBGGB',
  //   'GBGBGGB',
  //   'GBGBGGB',
  //   'GGGGGGG',
  //   'GGGGGGG',
  //   'GBGBGGB',
  //   'GBGBGGB',
  // ]
  [
    'BBBBBGGBGG',
    'GGGGGGGGGG',
    'GGGGGGGGGG',
    'BBBBBGGBGG',
    'BBBBBGGBGG',
    'GGGGGGGGGG',
    'BBBBBGGBGG',
    'GGGGGGGGGG',
    'BBBBBGGBGG',
    'GGGGGGGGGG',
  ]
]

const sampleOutput = [
  // 5,
  // 25,
  // 81,
  // 45,
  85
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const grid: string[] = input
  const actual = twoPluses(grid) 

  console.log({ expected, actual, pass: expected === actual })
}