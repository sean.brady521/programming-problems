const crypto = require('crypto')

function splitGrid (grid) {
  return grid.map(row => row.split(''))
}

function joinGrid (grid) {
  return grid.map(row => row.join(''))
}

function getFilledGrid (rows, cols, fill) {
  const filled = []
  for (let row = 0; row < rows; row++) {
    filled.push(new Array(cols).fill(fill))
  }
  return filled
}

function isBombOrNeighbour (x, y, grid) {
  const neighborAndSelfArr = [[x, y], [x + 1, y], [x - 1, y], [x, y + 1], [x, y - 1]]
  for (const pos of neighborAndSelfArr) {
    const [nX, nY]  = pos
    if (nX < 0 || nY < 0 || nX >= grid.length || nY >= grid[0].length) continue
    if (grid[nX][nY] === 'O') { 
      return true
    }
  }

  return false
}

function getNextState (grid) {
  const nextState = getFilledGrid(grid.length, grid[0].length, '.')
  for (let x = 0; x < grid.length; x++) {
    for (let y = 0; y < grid[0].length; y++) {
      const isDead = isBombOrNeighbour(x, y, grid)
      if (!isDead) {
        nextState[x][y] = 'O'
      }
    }
  }
  return nextState
}

function hashGrid (grid) {
  const gridStr = grid.join('')
  return crypto.createHash('md5').update(gridStr).digest('hex')
}

function bruteForceBomberMan (n, grid) {
  let gridState = splitGrid(grid)
  for (let currN = 3; currN <= n; currN += 2) {
    gridState = getNextState(gridState || grid)
  }
  return joinGrid(gridState)
}

function bomberMan (n, grid) {
  if (n % 2 === 0) {
    return joinGrid(getFilledGrid(grid.length, grid[0].length, 'O'))
  } 

  if (n < 10) return bruteForceBomberMan(n, grid)
  
  const gridOccurences = {}
  let gridState = splitGrid(grid)
  for (let currN = 1; currN <= n; currN += 2) {
    const hash = hashGrid(gridState)
    if (typeof gridOccurences[hash] !== 'undefined') {
      const cycleStart = gridOccurences[hash]
      const cycleEnd = currN - 2
      const stepsSinceStart = n - cycleStart

      const cycleLength = ((cycleEnd - cycleStart) / 2) + 1
      const oddsSinceStart = stepsSinceStart / 2

      const patternIndex = oddsSinceStart % cycleLength
      const desiredPatternN = cycleStart + 2 * patternIndex

      console.log({cycleStart, cycleEnd, stepsSinceStart, cycleLength, oddsSinceStart, patternIndex, desiredPatternN})

      return bruteForceBomberMan(desiredPatternN, grid)
    } else {
      gridOccurences[hash] = currN 
    }
    gridState = getNextState(gridState || grid)
  }

  console.log('AAAAAAAAAAAAAAAAH')

  return [['n'], ['u'], ['p']]
}

const sampleInput = [
  {
    n: 3,
    grid: [ 
      '.......', 
      '...O...', 
      '....O..', 
      '.......', 
      'OO.....', 
      'OO.....' 
    ]
  },
  // {
  //   n: 9,
  //   grid: [ 
  //     '.......',
  //     '...O.O.',
  //     '....O..',
  //     '..O....',
  //     'OO...OO',
  //     'OO.O...' 
  //   ]
  // }
]

const sampleOutput = [
  [
    'OOO.OOO',
    'OO...OO',
    'OOO...O',
    '..OO.OO',
    '...OOOO',
    '...OOOO',
  ],
  // [
  //   '.......',
  //   '...O.O.',
  //   '...OO..',
  //   '..OOOO.',
  //   'OOOOOOO',
  //   'OOOOOOO',
  // ]
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const { n, grid } = input
  const actual = bomberMan(n, grid) 

  console.log({ expected, actual, pass: expected.toString() === actual.toString() })
}