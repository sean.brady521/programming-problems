function canCompleteCircuit(gas: number[], cost: number[]): number {
  const diff = Array(cost.length, 0)
  for (let i = 0; i < cost.length; i++) {
    diff[i] = gas[(i + 1) % cost.length] - cost[i]
  }
    
  const minimumGasArr = Array(gas.length)
  minimumGasArr[0] = minimumGasRequiredForStation0(gas, cost, diff)

  console.log(minimumGasArr[0])
  for (let i = gas.length - 1; i > 0; i--) {
    minimumGasArr[i] = minimumGasArr[(i + 1) % gas.length] - diff[i]
  }

  console.log(minimumGasArr)

  for (let i = 0; i < gas.length; i++) {
    if (minimumGasArr[i] <= gas[i]) return i
  }

  return -1
};

function minimumGasRequiredForStation0 (gas: number[], cost: number[], diff: number[]): number {
  let originalGas = gas[0]
  while (true) {
    let currGas = originalGas
    let succeeded = true
    for (let i = 0; i < cost.length; i++) {
      if (currGas < cost[i]) { 
        succeeded = false
        break
      } else {
      }
      currGas += diff[i]
    }

    if (succeeded) {
      return originalGas
    }

    originalGas++
  }

}

const result = canCompleteCircuit([2,3,4], [3,4,3])
console.log(result)
