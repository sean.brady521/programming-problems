function surfaceArea (grid) {
  let exposedFaces = 0
  for (let rowIdx = 0; rowIdx < grid.length; rowIdx++) {
    for (let colIdx = 0; colIdx < grid[rowIdx].length; colIdx++) {
      const height = grid[rowIdx][colIdx]

      const neighbourCells = [
        { rowIdx: rowIdx - 1, colIdx }, // up
        { rowIdx: rowIdx, colIdx: colIdx + 1 }, // right
        { rowIdx: rowIdx - 1, colIdx }, // down
        { rowIdx: rowIdx, colIdx: colIdx - 1 }, // left
      ]

      // attach height of each neighbour
      for (const neighbour of neighbourCells) {
        neighbour.height = grid[neighbour.rowIdx]?.[neighbour.colIdx] || 0
      }

      exposedFaces += 2 // Top and bottom faces are always exposed
      // Move up block by block on the existing tile to assess exposed side faces on each
      for (let currHeight = 1; currHeight <= height; currHeight++) {
        for (const neighbour of neighbourCells) {
          if (neighbour.height < currHeight) { 
            exposedFaces++
          }
        }
      }
    }
  }

  return exposedFaces
}

const sampleInput = [
  [[1]],
  [
    [1,3,4],
    [2,2,3],
    [1,2,4]
  ]
]

const sampleOutput = [
  6,
  60
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const actual = surfaceArea(input) 

  console.log({ expected, actual, pass: expected === actual })
}
