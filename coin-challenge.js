function getWays (amount, denoms) {
  const resultCache = {}
  denoms.sort()
  for (let subAmount = 0; subAmount < amount; subAmount++) {
    getWaysInner(subAmount, denoms, resultCache)
  }
  console.log(resultCache)
  console.log(denoms)
  return getWaysInner(amount, denoms, resultCache)
}

function getWaysInner (amount, sortedDenoms, resultCache) {
  if (resultCache[amount]) return resultCache[amount]

  const minDenom = sortedDenoms[0]
  // const maxDenom = sortedDenoms[sortedDenoms.length - 1]

  let result = 0
  if (amount < minDenom) {
    result = 0
  } else if (amount === minDenom) {
    result = 1 
  } else {
    for (const denom of sortedDenoms) {
      if (denom > amount) break
      const invDenom = amount - denom
      if (!resultCache[invDenom]) continue

      if (invDenom === 0) {
        result = 1
      } else {
        const divisCount = getWaysInner(invDenom, sortedDenoms, resultCache)
        // if it's the minimum denom we can't divide that up anymore
        result = divisCount + 1
      }
      break
    }
  }

  resultCache[amount] = result
  return result
}

const sampleInput = [
  {
    amount: 3,
    denoms: [8, 3, 1, 2]
  },
  {
    amount: 4,
    denoms: [1, 2, 3]
  },
  {
    amount: 10,
    denoms: [2, 5, 3, 6]
  },
  {
    amount: 179,
    denoms: [24, 6, 48, 27, 36, 22, 35, 15, 41, 1, 26, 25, 4, 8, 14, 20, 9, 38, 34, 40, 45, 17, 33, 19, 5, 43, 2],
  },
]

const sampleOutput = [
  3,
  4,
  5,
  1283414971
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const { amount, denoms } = input
  const actual = getWays(amount, denoms) 

  console.log({ expected, actual, pass: expected === actual })
}
