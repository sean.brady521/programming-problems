function formingMagicSquare (origSquare) {
  console.log(origSquare)
}

const sampleInput = [
  [
    [1,1],
    [2,5],
    [3,3],
    [5,3],
    [3,2],
    [2,2]
  ],
  [
    [3,2],
    [2,5],
    [4,5]
  ],
]

const sampleOutput = [
  12.2,
  8.3
]

for (const input of sampleInput) {
  const result = formingMagicSquare(input)

  console.log({ result })
}
