function nonDivisibleSubset (nums, divisor) {
  // Build map that counts the occurences of each mod value that exists in the array
  const modResultCount = {}
  for (const num of nums) {
    const modResult = num % divisor

    if (!modResultCount[modResult]) modResultCount[modResult] = 0

    modResultCount[modResult]++
  }

  // Go through the pairs of mod values that exclude each other and pick biggest one
  let total = 0
  const excluded = {}
  for (const [modKey, count] of Object.entries(modResultCount)) {
    const modValue = parseInt(modKey)
    if (modValue === 0 || excluded[modValue]) continue

    const partnerMod = divisor - modValue
    if (modValue === partnerMod || excluded[partnerMod]) continue

    const partnerCount = modResultCount[partnerMod]
    if (!partnerCount || count >= partnerCount) {
      total += count
      excluded[partnerMod] = true
    } else {
      total += partnerCount
      excluded[modValue] = true
    }
  }

  // Check for case were we can add one from evenly divisible
  if (divisor % 2 === 0 && modResultCount[divisor / 2] > 0) {
    total++
  }

  // Check for case were we can add one from perfectly divisible
  if (modResultCount['0'] > 0) {
    total++
  }

  return total
}

const sampleInput = [
  {
    divisor: 4,
    nums: [19, 10, 12, 10, 24, 25, 22]
  },
  {
    divisor: 3,
    nums: [1, 7, 2, 4]
  },
  {
    divisor: 7,
    nums: [278, 576, 496, 727, 410, 124, 338, 149, 209, 702, 282, 718, 771, 575, 436]
  }
]

for (const input of sampleInput) {
  const { divisor, nums } = input
  const result = nonDivisibleSubset(nums, divisor)

  console.log({ divisor, nums, result })
}
