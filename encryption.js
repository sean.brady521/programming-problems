function encryption (str) {
  const strippedStr = str.replace(/\s+/g, '')
  const len = strippedStr.length
  const sqrt = Math.sqrt(len)
  const sqrtFloor = Math.floor(sqrt)
  const sqrtCeil = Math.ceil(sqrt)

  let rows, cols
  if (sqrtFloor * sqrtFloor >= len)  {
    rows = sqrtFloor
    cols = sqrtFloor
  } else if (sqrtFloor * sqrtCeil >= len) {
    rows = sqrtFloor
    cols = sqrtCeil
  } else if (sqrtCeil * sqrtCeil >= len) {
    rows = sqrtCeil 
    cols = sqrtCeil
  } else {
    console.log('WTF!')
    console.log({sqrtCeil, sqrtFloor, sqrt, len})
  }

  let encryptedStr = ''
  for (let c = 0; c < cols; c++) {
    for (let r = 0; r < rows; r++) {
      const idx = r * cols + c
      if (idx >= len) continue
      encryptedStr += strippedStr[idx]
    }
    if(c !== cols - 1) encryptedStr += ' '
  }
  return encryptedStr
}

const sampleInput = [
  'if man was meant to stay on the ground god would have given us roots',
  'have a nice day',
  'feed the dog',
  'chillout'
]

const sampleOutput = [
  'imtgdvs fearwer mayoogo anouuio ntnnlvt wttddes aohghn sseoau',
  'hae and via ecy',
  'fto ehg ee dd',
  'clu hlt io'
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i] 
  const actual = encryption(input)
  console.log({ input, expected, actual })
}
