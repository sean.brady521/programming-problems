function perm(xs) {
  let ret = []

  for (let i = 0; i < xs.length; i = i + 1) {
    let rest = perm(xs.slice(0, i).concat(xs.slice(i + 1)));

    if(!rest.length) {
      ret.push(xs[i])
    } else {
      for(let j = 0; j < rest.length; j = j + 1) {
        ret.push(xs[i] + rest[j])
      }
    }
  }
  return ret;
}

function trimPerms (perms, str) {
  const trimmed = []
  for (const perm of perms) {
    if (perm > str) trimmed.push(perm)
  }
  return trimmed
}

function biggerIsGreater (str) {
  const wordLen = str.length

  for (let subLen = 2; subLen <= wordLen; subLen++) {
    const subStr = str.slice(-subLen)
    const allPerms = perm(subStr)
    const perms = trimPerms(allPerms, subStr)
    if (!perms.length) continue
    let minPerm = perms[0]
    for (const perm of perms) {
      if (perm < minPerm) minPerm = perm
    }
    return str.slice(0, str.length - subLen) + minPerm
  }

  return 'no answer'
}

const sampleInput = [
  'ab',
  'bb',
  'hefg',
  'dhck',
  'dkhc',
  'dcbb'
]

const sampleOutput = [
  'ba',
  'no answer',
  'hegf',
  'dhkc',
  'hcdk',
  'no answer'
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const result = biggerIsGreater(input)

  console.log({ result, expected })
}
