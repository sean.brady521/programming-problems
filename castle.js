function mutChangePriority(array, gridRep, elemIdx, newPriority) {
  for (let i = 0; i < array.length; i++) {
    const { x, y } = array[i]
    const { distFromSrc, distToDest } = gridRep[x][y]
    const priority = distFromSrc + distToDest
    // console.log({priority})
    if (priority >= newPriority) {
      mutMove(array, elemIdx, i)
      return
    }
  }
}

function mutMove(array, from, to) {
  if( to === from ) return array;

  const target = array[from];                         
  const increment = to < from ? -1 : 1;

  for (let k = from; k != to; k += increment) {
    array[k] = array[k + increment]
  }
  array[to] = target
  return array;
}

function minDistance (start, goal) {
  const dx = Math.abs(start.x - goal.x)
  const dy = Math.abs(start.y - goal.y)
  return dx + dy
}

function getNeighbours (grid, pos) {
  const { x, y } = pos
  const possibleLocs = [
    { x: x - 1, y },
    { x: x + 1, y },
    { x, y: y - 1 },
    { x, y: y + 1 }
  ]
  const neighbours = []
  for (const loc of possibleLocs) {
    const val = grid[loc.y]?.[loc.x]
    if (val && val !== 'x') neighbours.push(loc)
  }
  return neighbours
}

function minimumMoves (grid, startX, startY, goalX, goalY) {
  const gridRep = []
  const infiniteDist = grid.length * grid.length // maximum possible shortest path covering every cell
  const priorityQueue = [{x: startX, y: startY}]
  for (let x = 0; x < grid.length; x++) {
    gridRep.push([])
    for (let y = 0; y < grid.length; y++) {
      gridRep[x].push({
        distFromSrc: infiniteDist,
        distToDest: minDistance({ x, y }, { x: goalX, y: goalY }),
        via: null
      })
      if (grid[y][x] !== 'x' && !(startX === x && startY === y)) { 
        priorityQueue.push({
          x,
          y,
        })
      }
    }
  }

  gridRep[startX][startY].distFromSrc = 0

  while(true) {
    const currNode = priorityQueue.shift()
    const { x, y } = currNode
    const { distFromSrc, explored } = gridRep[x][y]

    if (explored) continue
    gridRep[x][y].explored = true 

    console.log(`exploring (${x},${y}), dst: ${distFromSrc}`)
    if (x === goalX && y === goalY) return distFromSrc

    const neighbours = getNeighbours(grid, currNode)
    console.log(`adding neighbours to queue`)
    for (const neighbour of neighbours) {
      const neighbourVals = gridRep[neighbour.x][neighbour.y]
      if (neighbourVals.explored) continue
      console.log(`(${neighbour.x},${neighbour.y})`)
      if (neighbourVals.distFromSrc > distFromSrc + 1) {
        console.log(`shorter path to (${neighbour.x},${neighbour.y}) found through, dst: ${distFromSrc + 1}`)
        neighbourVals.distFromSrc = distFromSrc + 1
        neighbourVals.via = { x, y }
        const neighbourIdx = priorityQueue.findIndex(elm => elm.x === neighbour.x && elm.y === neighbour.y)
        // console.log(`changing neighbour idx`)
        // console.log(`before: ${neighbourIdx}`)
        if (neighbourIdx === -1) continue
        mutChangePriority(priorityQueue, gridRep, neighbourIdx, neighbourVals.distFromSrc + neighbourVals.distToDest)
        const neighbourIdxNew = priorityQueue.findIndex(elm => elm.x === neighbour.x && elm.y === neighbour.y)
        console.log(`changing neighbour idx ${neighbourIdx} -> ${neighbourIdxNew}`)
        // console.log(`after: ${neighbourIdxNew}`)
      }
    }
  }
}

const sampleInput = [
  // {
  //   grid: [
  //     '.x.',
  //     '.x.',
  //     '...'
  //   ],
  //   startX: 0,
  //   startY: 0,
  //   goalX: 0,
  //   goalY: 2
  // },
  // {
  //   grid: [
  //     '...',
  //     '.x.',
  //     '...'
  //   ],
  //   startX: 0,
  //   startY: 0,
  //   goalX: 1,
  //   goalY: 2
  // },
  {
    grid: [
      '...',
      '.x.',
      '.x.'
    ],
    startX: 2,
    startY: 0,
    goalX: 0,
    goalY: 2
  },
]

for (const input of sampleInput) {
  const result = minimumMoves(input.grid, input.startX, input.startY, input.goalX, input.goalY)

  console.log({ result })
}