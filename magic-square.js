function formingMagicSquare (origSquare) {
  console.log(origSquare)
}

const sampleInput = [
  [
    [5, 3, 4],
    [1, 5, 8],
    [6, 4, 2]
  ],
  [
    [4, 9, 2],
    [3, 5, 7],
    [8, 1, 5]
  ],
  [
    [4, 8, 2],
    [4, 5, 7],
    [6, 1, 6]
  ]
]

for (const input of sampleInput) {
  const result = formingMagicSquare(input)

  console.log({ result })
}
