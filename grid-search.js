// Find starting pattern
// Create an "allowedStartingIdxs" array from all occurences of starting pattern in given line
// Start iterating downwards, check if the pattern we need is at each of the "allowed starts"
// If the pattern doesn't exist at an allowed idx then that idx is no longer "allowed" and is removed from our list
// Once we hit 0 allowed indices it's game over and we go back to looking for a starting line

function getAllStartingIndicies (row, pattern) {
  let startingIndicies = new Set()
  for (let i = 0; i < row.length; i++) {
    let idx = row.indexOf(pattern, i)
    if (idx !== -1) startingIndicies.add(idx)
  }
  return startingIndicies
}

function isStringAtIdx (row, idx, subStr) {
  return row.slice(idx, idx + subStr.length) === subStr
}

function gridSearch (grid, pattern) {
  for (let rowIdx = 0; rowIdx < grid.length; rowIdx++) {
    const row = grid[rowIdx]
    if (row.includes(pattern[0])) {
      console.log(`Found starting row match at row ${rowIdx}`)
      const remainingRowCount = grid.length - rowIdx - 1
      console.log(`${remainingRowCount} rows in grid left to match ${pattern.length - 1} pattern rows`)
      if (remainingRowCount < pattern.length - 1) continue

      const allowedStartingIndicies = getAllStartingIndicies(row, pattern[0])
      console.log(`allowed starting indicies: ${Array.from(allowedStartingIndicies.keys())}`)

      for (let patIdx = 1; patIdx < pattern.length; patIdx++) {
        console.log(`checking ${patIdx} row(s) down`)
        const subRowIdx = rowIdx + patIdx
        for (const allowedIdx of allowedStartingIndicies) {
          if (!isStringAtIdx(grid[subRowIdx], allowedIdx, pattern[patIdx])) {
            console.log(`No match starting at ${allowedIdx}`)
            allowedStartingIndicies.delete(allowedIdx)
            console.log(`allowed starting indicies is now: ${Array.from(allowedStartingIndicies.keys())}`)
            if (allowedStartingIndicies.size === 0) break
          }
        }

        if (allowedStartingIndicies.size === 0) break
      }

      if (allowedStartingIndicies.size > 0) return 'YES'
    }
  }
  return 'NO'
}

const sampleInput = [
  {
    grid: [
      '1234567890',
      '0987654321',
      '1111111111',
      '1111111111',
      '2222222222'
    ],
    pattern: [
      '876543',
      '111111',
      '111111'
    ]
  }
]

const sampleOutput = [
  'YES'
]

for (let i = 0; i < sampleInput.length; i++) {
  const input = sampleInput[i]
  const expected = sampleOutput[i]

  const { grid, pattern } = input
  const actual = gridSearch(grid, pattern) 

  console.log({ expected, actual, pass: expected === actual })
}
