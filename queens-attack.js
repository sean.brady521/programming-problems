function createObstacleMap (obstacles) {
  const map = {}
  for (const obstacle of obstacles) {
    const [r, c] = obstacle
    if (!map[r]) map[r] = {}
    map[r][c] = true
  }
  return map
}

function attackDirection (n, rQ, cQ, obstacleMap, dirR, dirC) {
  console.log(`Attacking direction ${dirR},${dirC} from ${rQ},${cQ}`)
  let squaresHit = 0
  let currR = rQ
  let currC = cQ

  while (currC <= n && currC > 0 && currR <= n && currR > 0) {
    // console.log(`${currR},${currC}`)
    if (obstacleMap[currR] && obstacleMap[currR][currC]) {
      // console.log('Hit obstacle')
      break
    }
    squaresHit++
    currR += dirR
    currC += dirC
  }

  // console.log(`attacked ${Math.max(0, squaresHit - 1)} squares`)
  return Math.max(0, squaresHit - 1)
}

function queensAttack (n, _, rQ, cQ, obstacles) {
  const obstacleMap = createObstacleMap(obstacles)

  const directions = [
    [1, 0], // up
    [-1, 0], // down
    [0, -1], // left
    [0, 1], // right
    [1, 1], // up-right
    [-1, 1], // down-right
    [-1, -1], // down-left
    [1, -1] // up-left
  ]

  let squaresHit = 0
  for (const [dirR, dirC] of directions) {
    const hitSquares = attackDirection(n, rQ, cQ, obstacleMap, dirR, dirC)
    squaresHit += hitSquares
  }
  return squaresHit
}

const sampleInput = [
  {
    n: 4,
    rQ: 4,
    cQ: 4,
    obstacles: []
  },
  {
    n: 5,
    rQ: 4,
    cQ: 3,
    obstacles: [[5, 5], [4, 2], [2, 3]]
  },
  {
    n: 1,
    rQ: 1,
    cQ: 1,
    obstacles: []
  }
]

for (const input of sampleInput) {
  const { n, rQ, cQ, obstacles } = input
  console.log(queensAttack(n, null, rQ, cQ, obstacles))
}
