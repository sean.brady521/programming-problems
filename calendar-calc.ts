import moment from 'moment'

function printMonthsWith3Slips(firstSlip: moment.Moment, mEndDate: moment.Moment) {
  const curr = firstSlip.clone()
  let currentMonth = curr.month()
  let fortnightsInMonth = 0
  while(curr.isBefore(mEndDate)) {
    if (curr.month() === currentMonth) fortnightsInMonth++
    else { 
      fortnightsInMonth = 0
      currentMonth = curr.month()
    }

    if (fortnightsInMonth === 2) console.log(curr.format('YYYY MMM'))

    // console.log(`${curr.format('YYYY-MM-DD')}   |    ${fortnightsInMonth}`)

    curr.add(2, 'weeks')


  }

}

printMonthsWith3Slips(moment('2022-07-19'), moment('2035-01-01'))